/**
 * Message Model
 */
export interface Message {
    uid: string,
    author: string,
    message: string,
    date?: string,
    image: string
}