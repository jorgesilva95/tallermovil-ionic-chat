import { AngularFireStorage } from '@angular/fire/storage';
import { AlertController, normalizeURL } from 'ionic-angular';
import { Injectable } from '@angular/core';

import { AngularFireDatabase, AngularFireAction, DatabaseSnapshot } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';

import { Message } from '../../common/message';
import Utils from '../../common/utils';

/**
 * Message Service, connect a FireDatabase and get, create and delete actions.
 */

@Injectable()
export class MessageProvider {

  //Messages array
  messages: Observable<AngularFireAction<DatabaseSnapshot<Message>>[]>;
  //User object
  user: any;

  constructor(private storage: AngularFireStorage, private _db: AngularFireDatabase, private alertCtrl: AlertController) {
    //Get messages in DB
    this.messages = _db.list<Message>('messages').snapshotChanges();
  }

  /**
   * Get a messages var
   */
  getMessages(){
    return this.messages;
  }

  /**
   * Delete a message to DB
   * @param msg message to delete
   */
  delete(msg: any) {
    this.alertCtrl.create({
      title: 'Are you sure?',
      buttons: [
        {
          text: 'YES',
          handler: data => {
            this._db.list('messages/' + msg.key).remove()
              .then(data => {
                Utils.alert('Great!', 'The message is erased', this.alertCtrl);
              })
              .catch(error => {
                Utils.alert('An error has ocurred :(!', error.message, this.alertCtrl);
              });
          }
        },
        {
          text: 'NO'
        }
      ]
    }).present();
  }

  /**
   * Send a message to DB
   * @param msg msg to send
   * @param user user own
   */
  send(msg: string, img: string, user: any) {
    if (!msg && !img) return;

    if(!msg) msg = '';
    if(!img) img = '';

    this._db.list('messages').push({
      uid: user.uid,
      author: user.displayName,
      message: msg,
      image: img,
      date: new Date().toString()
    });
  }

}
