import { AngularFireAuth } from '@angular/fire/auth';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';

import Utils from '../../common/utils';

/**
 * Generated class for the RegisterPage page.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  email: string;
  password: string;
  confirm_password: string;

  constructor(private fire: AngularFireAuth, private _db: AngularFireDatabase, 
    public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  /**
   * Register a new user.
   */
  register() {
    //Check if password and confirm_password equals
    if(this.password != this.confirm_password){
      Utils.alert("An error has ocurred :(", 'The passwords not match', this.alertCtrl);
      return;
    }
    //Create a user whit email and password, if register is successfull show a message and send to LoginPage
    //Otherwise show a error message and nothing
    this.fire.auth.createUserWithEmailAndPassword(this.email, this.password)
      .then(data => {
        Utils.alert("Congrats!", "Register succesfull", this.alertCtrl);
        this.navCtrl.remove(1);
      })
      .catch(error => {
        Utils.alert("An error has ocurred :(", error.message, this.alertCtrl);
      });
  }

}
