import { AlertController } from 'ionic-angular';

/**
 * Utils class
 */
export default class Utils {
    /**
     * Alert. With a Title and message dinamic
     */
    static alert(title: string, message: string, alertCtrl: AlertController){
        alertCtrl.create({
          title: title,
          subTitle: message,
          buttons: ['OK']
        }).present();
    }
}