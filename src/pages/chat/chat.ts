import { AngularFireAuth } from '@angular/fire/auth';
import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, ActionSheetController } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';
import firebase from 'firebase';
import { Content } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { MessageProvider } from './../../providers/message/message';
import { LoginPage } from '../login/login';

/**
 * This class "controller" a chat.html web. Create, Read, Delete Messages Actions
 */

@Component({
  selector: 'page-home',
  templateUrl: 'chat.html',
})
export class ChatPage {
  @ViewChild(Content) content: Content;

  user: any;
  //messages
  messages: any;

  // New message
  newMessage: string;
  //current image loadded
  newImage: string;
  //Disable input message
  newMessageDisabled: boolean=false;

  constructor(private camera: Camera,
    private messagesService: MessageProvider,
    public alertCtrl: AlertController, public navCtrl: NavController, public actionSheetCtrl: ActionSheetController,
    private fire: AngularFireAuth, private _db: AngularFireDatabase) {
    //Check if user logged
    this.fire.authState.subscribe(user => {
      //If not logged send to LoginPage
      if (!user) {
        this.navCtrl.setRoot(LoginPage);
        return;
      }
      this.user = user;
      //If current user not have a displayName or null, create them
      if (user.displayName == null) {
        this.createName(alertCtrl, fire);
      }
    });
    //Set messages var
    this.messages = messagesService.getMessages();
    this.scrollToBottom();
  }

  actionPicture(){
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Select a option',
      buttons: [
        {
          text: 'Take a photo',
          handler: () => {
            this.getPicture(1);
          }
        },{
          text: 'Search in gallery ',
          handler: () => {
            this.getPicture(0);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  

  /**
   * Get a picture
   */
  getPicture(sourceType: number) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType:sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.newImage = base64Image;
    }, (err) => {
      console.log(err.message);
    });
  }

  /**
   * Create a displayName alert.
   * @param alertCtrl AlertController
   * @param fire AngularFireAuth, this get a current user auth
   */
  createName(alertCtrl: AlertController, fire: AngularFireAuth) {
    alertCtrl.create({
      title: 'Insert your Display Name',
      inputs: [
        {
          name: 'displayName',
          value: this.user.email,
          placeholder: 'Bieber'
        }
      ],
      buttons: [
        {
          text: 'OK',
          handler: data => {
            firebase.auth().onAuthStateChanged(function (user) {
              user.updateProfile({
                displayName: data.displayName,
                photoURL: ''
              });
            })
          }
        }
      ]
    }).present();
  }

  /**
   * Delete a message
   * @param msg message to delete 
   */
  deleteMessage(msg: any) {
    this.messagesService.delete(msg);
  }

  /**
   * Send a message
   */
  sendMessage() {
    if (!this.newMessage && !this.newImage) return;

    //Use a MessageProvider to send a message
    this.messagesService.send(this.newMessage, this.newImage, this.user);

    this.newMessage = '';
    this.newImage = '';
    this.scrollToBottom();
  }

  /**
   * Scroll to Bottom of page
   */
  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 100);
  }


}
