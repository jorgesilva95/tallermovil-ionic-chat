import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { RegisterPage } from './../register/register';
import { ChatPage } from './../chat/chat';
import { AngularFireAuth } from '@angular/fire/auth';

import Utils from '../../common/utils';
import firebase from 'firebase';

/**
 * Generated class for the LoginPage page.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild("username") username;
  @ViewChild("password") password;

  constructor(private fire:AngularFireAuth, 
    public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
  }

  /**
   * Sign in with a username, password
   */
  signIn(){
    firebase.auth().signInWithEmailAndPassword(this.username.value, this.password.value)
    .then(data => {
      this.navCtrl.setRoot( ChatPage );
    })
    .catch(error =>{
      Utils.alert("An error has ocurred :(", error.message, this.alertCtrl);
    })
  }
  
  /**
   * Send to RegisterPage
   */
  signUp(){
    this.navCtrl.push(RegisterPage);
  }
}
