import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

// Nota: (1) Importa módulos de firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { Camera } from '@ionic-native/camera';

// Nota (2) Credenciales y configuración inicial de firebase
export const firebaseConfig = {
  apiKey: "AIzaSyCGZu8uKDZTvWYkQwtUTaJ5qFONEpnB2no",
  authDomain: "tallermovil-chat.firebaseapp.com",
  databaseURL: "https://tallermovil-chat.firebaseio.com",
  projectId: "tallermovil-chat",
  storageBucket: "tallermovil-chat.appspot.com",
  messagingSenderId: "638642083997"
};

// Importa páginas (custom elements)
import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from './../pages/register/register';
import { ChatPage } from './../pages/chat/chat';
import { MessageProvider } from '../providers/message/message';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    RegisterPage,
    ChatPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    // Nota (3) Importa módulos
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    RegisterPage,
    ChatPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // Nota (4) Importa provider firebase database
    AngularFireDatabase,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Camera,
    MessageProvider
  ]
})
export class AppModule { }
